clear all ;
clf ;
%% Parameters
L         = 1                   ; % Layers
n         = [3 ones(1,L) .* 6 ] ; % n0 .. n5
eta_att   = 0.01              ;
eta_rep   = 0.002             ;
epochs    = 200               ;
random_ep = 20                ;

%% Load the files
[X,Y] = textread('hw2pt.dat','%f %f');
rownum = length(X) ;
tempmat = textread('hw2class.dat','%d') ;
classMat  = zeros(rownum) ;
YMat  = zeros(L+1,rownum,max(n)) ; % L0 .. L5
for i=1:rownum
    classMat(i,1:rownum) = tempmat( (i-1)*rownum+1 : (i)*rownum )'  ; 
    YMat(1,i,1:3) = [X(i) Y(i) -1]  ;
end
clear X Y tempmat ;

%% SOM Perceptron

for m=1:L
    Weight{m} = -2.0 + 4.0 .* rand( n(m) , n(m+1)-1 ) ; % maybe it should be random
    for epoch = 1:epochs
        if ( epoch <= random_ep )
            % Do random select
            [Yp,Xp,Yq,Xq,~] = select_x(m,YMat,Weight,classMat,3) ;
            [Yr,Xr,Ys,Xs,~] = select_x(m,YMat,Weight,classMat,2) ;  
            [~ ,~ ,~ ,~ ,dis_far(m,epoch)] = select_x(m,YMat,Weight,classMat,1) ;
            [~ ,~ ,~ ,~ ,dis_nea(m,epoch)] = select_x(m,YMat,Weight,classMat,0) ;            
        else
            % Do max or min distance select
            [Yp,Xp,Yq,Xq,dis_far(m,epoch)] = select_x(m,YMat,Weight,classMat,1) ;
            [Yr,Xr,Ys,Xs,dis_nea(m,epoch)] = select_x(m,YMat,Weight,classMat,0) ;
        end
        for i=1:n(m)
            for j=1:(n(m+1)-1)                
                Weight{m}(i,j) = Weight{m}(i,j)                                 ...
                    - eta_att * (                                               ...
                        +   Xp(i) * ( Yp(j) - Yq(j) ) * ( Yp(j) - Yp(j)^2 )     ...
                        -   Xq(i) * ( Yp(j) - Yq(j) ) * ( Yq(j) - Yq(j)^2 )     ...
                    )                                                           ...
                    - eta_rep * (                                               ...
                        -   Xr(i) * ( Yr(j) - Ys(j) ) * ( Yr(j) - Yr(j)^2 )     ...
                        +   Xs(i) * ( Yr(j) - Ys(j) ) * ( Ys(j) - Ys(j)^2 )     ...
                    );
            end
        end
        if ( mod(epoch,10) == 0 )
            disp(['epoch=' num2str(epoch)]);
        end
    end
    
    for i = 1 : rownum
        tempX(1:n(m),1) =  YMat(m,i,1:n(m)) ;
        tempY = sigmoid ( Weight{m}' * tempX ) ;
        tempY = -1 + 2 .* tempY ; 
        YMat(m+1,i,1:n(m+1)) = [tempY' -1]  ;
    end
end

hold on
plot(1:epoch,dis_far(1,1:epoch),'r');
plot(1:epoch,dis_nea(1,1:epoch),'b');
set(gca,'YScale','log');




















