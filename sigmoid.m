function out_v = sigmoid(in_v)
    out_v = 1.0 ./ ( 1.0 + exp(-in_v) ) ;
end