function [Yp,Xp,Yq,Xq,recDistance] = select_x(Layer,YMat,Weight,classMat,mode)
    % mode : imply the same class(1) or different class(0) 
    %        Random diff(2) , Random same (3) 
    [lenData , ~ ] = size(classMat) ;
        
    if ( mode == 1 )
        recDistance = 0.0 ;
    elseif ( mode == 0 )
        recDistance = +99999999.9 ;
    else
        while ( 1 )
            random_i = randi([1,lenData],1) ;
            random_j = randi([1,lenData],1) ;
            if ( random_i ~= random_j && classMat(random_i,random_j) == mode-2 )
                if ( random_i > random_j )
                    temp = random_j ;
                    random_j = random_i ;
                    random_i = temp ;
                end
                break ;
            end
        end
    end
    
    recD_index1 = 0   ;
    recD_index2 = 0   ;

    for i = 1 : lenData
        for j = i : lenData 
            if ( i == j )
                continue;
            end
            if ( classMat(i,j) ~= mode && mode < 2 )
                continue
            end
            [Xlength,~] = size(Weight{Layer}) ;
            Xi(1:Xlength,1) = YMat(Layer,i,1:Xlength) ; % Previous Layer Output
            Xj(1:Xlength,1) = YMat(Layer,j,1:Xlength) ; % Previous Layer Output
            Yi = sigmoid ( Weight{Layer}' * Xi  ) ;
            Yj = sigmoid ( Weight{Layer}' * Xj  ) ;
            distance = norm( Yi - Yj )^2 ;
            if ( ( mode == 1 && ( distance > recDistance ) ) || ...
                 ( mode == 0 && ( distance < recDistance ) ) || ...
                 ( mode >= 2 && ( i == random_i && j == random_j ) ) )
                    recDistance = distance ;
                    Yp  = Yi ;
                    Xp  = Xi ;
                    Yq  = Yj ;
                    Xq  = Xj ;                    
            end
        end
    end
end